package com.example.roomdemoapp.dto;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import com.example.roomdemoapp.model.Task;;

public class FavoriteRepository {

    private static FavoriteRepository mInstance;
    private static AppDatabase favoriteDatabase;

    private FavoriteRepository(Context context) {

    }

    public static synchronized FavoriteRepository getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new FavoriteRepository(mCtx);
            favoriteDatabase = Room.databaseBuilder(mCtx, AppDatabase.class, "mls")
                    .allowMainThreadQueries()
                .build();
        }
        return mInstance;
    }

    public AppDatabase getAppDatabase() {
        return favoriteDatabase;
    }


    @SuppressLint("StaticFieldLeak")
    public void insertTask(final Task task) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                favoriteDatabase.taskDao().insert(task);
                return null;
            }
        }.execute();
    }


}
