package com.example.roomdemoapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.example.roomdemoapp.dto.FavoriteRepository;
import com.example.roomdemoapp.model.Task;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText editTextTask, editTextDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextTask = findViewById(R.id.editTextTask);
        editTextDesc = findViewById(R.id.editTextDesc);

        findViewById(R.id.button_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveTask();
            }
        });

        findViewById(R.id.button_get).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });

    }

    private void saveTask() {
        final String sTask = editTextTask.getText().toString().trim();
        final String sDesc = editTextDesc.getText().toString().trim();

        if (sTask.isEmpty()) {
            editTextTask.setError("Task required");
            editTextTask.requestFocus();
            return;
        }

        if (sDesc.isEmpty()) {
            editTextDesc.setError("Desc required");
            editTextDesc.requestFocus();
            return;
        }

        Task task = new Task();
        task.setTask(sTask);
        task.setDesc(sDesc);
        FavoriteRepository.getInstance(getApplicationContext()).getAppDatabase()
                .taskDao()
                .insert(task);
    }

    public void getData(){

       List<Task> tasks = FavoriteRepository.getInstance(getApplicationContext()).getAppDatabase()
                .taskDao()
                .getAll();

       for(int i = 0; i < tasks.size(); i++){
           Toast.makeText(this, ""+tasks.get(0).getTask(), Toast.LENGTH_SHORT).show();
       }

    }

}
